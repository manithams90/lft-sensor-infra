# infrastructure for sensor data processing  (Terraform)

This repository provides a guide and configuration files for infrastructure provisioning using terraform.

## Features
- `images/`: Docker files and the relelvant source code to run the sensor image.
- `terraform/`: Directory containing terraform configuration.

## Prerequisites
Ensure you have the following prerequisites installed:
- [gcloud](https://cloud.google.com/sdk/docs/install) - (latest)
- [Terraform](https://developer.hashicorp.com/terraform/install?product_intent=terraform) (version v1.7.4 or later)


## Implementation
Terraform codebase is structured as following in order to facilitate provision of infra to multiple enviroments and multiple regions.
````
    /terraform
    ├── dev
    │   ├── europe-west3
    │   │            ├── main.tf
    │   │            ├── provider.tf
    │   │            └── variable.tf
    │   ├── europe-west6
    │   │            ├── main.tf
    │   │            ├── provider.tf
    │   │            └── variable.tf
    │   └── global
    │          ├── main.tf
    │          ├── provider.tf
    │          └── variable.tf
    │   
    ├── global
    │   ├── main.tf
    │   ├── provider.tf
    │   └── variables.tf
    │
    ├── modules
    │   ├── artifact-registry-repository
    │   │                   ├── main.tf
    │   │                   └── variable.tf
    │   ├── pubsub
    │   │       ├── main.tf
    │   │       └── variable.tf
    │   ├── sensor
    │   │       ├── main.tf
    │   │       └── variable.tf
    │   │
    │.  .              
    └── production
            ├── europe-west3
            ├── europe-west6
            └── global
    ...
````
## Provision Infrastructure with Terraform
1. **Global resources**: Navigate to the `terraform/global/` directory and execute the terraform commands:

2. **Docker images**: Upon creating the `Global` resources navigate to the `images/` directory and execute the following commands tp push the built image to registry:
- Note: Makesure to update the registry name based on the globally provisioned artifact-registry-repository.
````
docker build -t sensor .
docker tag sensor europe-docker.pkg.dev/lht-cloud-challenge-418023/sensor/sensor
docker push europe-docker.pkg.dev/lht-cloud-challenge-418023/sensor/sensor
```` 

3. **Environment specific global resources**: Create the environment specific global resources such as VPC, PubSub topic.
    Eg: In `terraform/dev/global/` folder contains the tf config related to provison dev environment specific global resources

4. **Deploy sensor infra**: Under specific environment you can deploy the infra related to sensor data pipeline in different regions. Make sure you have correct values under `var.tf` and `main.tf`
    Eg: In `terraform/dev/europe-west3/` folder contains the tf config related to provison sensor data pipeline infra to `dev` environment in `europe-west3` region 

Use following commands to run terraform scripts 
```
terraform init
terraform plan
terraform apply   
```