terraform {
  required_version = ">= 1.7"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 5.21.0"
    }

    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 5.21.0"
    }
  }
}

provider "google" {
  # Credentials are stored as a variable set in a dedicated workspace.
  project = var.project_id
  region  = var.region
}

provider "google-beta" {
  # Credentials are stored as a variable set in a dedicated workspace.
  project = var.project_id
  region  = var.region
}
