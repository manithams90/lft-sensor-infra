locals {
  # Common lables to be assigned to all resources
  default_lables = {
    project = var.project_name
    env     = var.environment
    managed = "terraform"
  }
}

module "pubsub" {
  source       = "../../modules/pubsub"
  topic        = format("%s-sensor-data", var.environment)
  project_id   = var.project_id
  topic_labels = local.default_lables
}

module "vpc" {
  source       = "terraform-google-modules/network/google"
  version      = "9.0.0"
  project_id   = var.project_id
  network_name = format("%s-%s-vpc", var.resource_group_name, var.environment)

  subnets = []
}