locals {
  # Common lables to be assigned to all resources
  default_lables = {
    project = var.project_name
    env     = var.environment
    managed = "terraform"
  }
}

module "sensor" {
  source      = "../../modules/sensor"
  location    = var.region
  project_id  = var.project_id
  labels      = local.default_lables
  topic       = data.terraform_remote_state.global.outputs.pubsub_topic
  pubsub_id   = data.terraform_remote_state.global.outputs.pubsub_id
  environment = var.environment
}

module "subnet" {
  source                = "../../modules/subnets"
  project_id            = var.project_id
  network_name          = data.terraform_remote_state.global.outputs.network_name
  subnet_ip             = "10.3.0.0/16"
  subnet_region         = var.region
  subnet_private_access = true
  environment           = var.environment
}