variable "project_id" {
  description = "ID of the Google Cloud project"
  default     = "lht-cloud-challenge-418023"
  type        = string
}

variable "project_name" {
  description = "Name of the Google Cloud project"
  default     = "lht-cloud-challenge"
  type        = string
}

variable "region" {
  description = "Region of the resources"
  default     = "europe-west6"
  type        = string
}

variable "resources_label" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}

variable "environment" {
  description = "Infra Environment "
  default     = "prod"
  type        = string
}