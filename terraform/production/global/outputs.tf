output "pubsub_id" {
  value = module.pubsub.pubsub_id
}

output "pubsub_topic" {
  value = module.pubsub.pubsub_topic
}

output "network_name" {
  value = module.vpc.network_name
}