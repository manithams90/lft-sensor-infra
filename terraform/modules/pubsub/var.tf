variable "project_id" {
  description = "ID of the Google Cloud project"
  default     = "lht-cloud-challenge-418023"
  type        = string
}

variable "project_name" {
  description = "Name of the Google Cloud project"
  default     = "lht-cloud-challenge"
  type        = string
}

variable "topic_labels" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}

variable "environment" {
  description = "Infra Environment "
  default     = "prod"
  type        = string
}

variable "resource_group_name" {
  description = "The group name of resources"
  default     = "sensor"
  type        = string
}

variable "topic" {
  description = "Infra Environment "
  default     = "prod"
  type        = string
}