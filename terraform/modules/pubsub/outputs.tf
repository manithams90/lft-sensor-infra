output "pubsub_id" {
  value = module.pubsub.id
}

output "pubsub_topic" {
  value = module.pubsub.topic
}