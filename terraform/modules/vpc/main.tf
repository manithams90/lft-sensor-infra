module "vpc" {
  source       = "terraform-google-modules/network/google"
  version      = "9.0.0"
  project_id   = var.project_id
  network_name = format("%s-%s-vpc", var.resource_group_name, var.environment)

  subnets = []
}