variable "project_id" {
  description = "ID of the Google Cloud project"
  type        = string
}

variable "resources_label" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}

variable "environment" {
  description = "Infra Environment"
  type        = string
}

variable "resource_group_name" {
  description = "The group name of resources"
  type        = string
}