resource "google_cloud_scheduler_job" "job" {
  name             = format("%s-%s-invoker-job", var.environment, var.location)
  description      = "HTTP invoker job for sensor"
  schedule         = "*/5 * * * *"
  time_zone        = "Europe/Berlin"
  attempt_deadline = "320s"
  region           = var.location

  http_target {
    http_method = "GET"
    uri         = format("%s/send_report", google_cloud_run_service.default.status.0.url) #google_cloud_run_service.default.status.0.url/send_report

    oidc_token {
      service_account_email = google_service_account.sensor.email
    }
  }

  depends_on = [
    google_cloud_run_service.default
  ]
}