resource "google_cloud_run_service" "default" {
  name     = format("%s-%s-sensor", var.environment, var.location)
  location = var.location

  template {
    spec {
      containers {
        image = var.image #"europe-docker.pkg.dev/lht-cloud-challenge-418023/sensor/sensor:latest"
        env {
          name  = "TOPIC_PATH"
          value = var.pubsub_id
        }
        env {
          name  = "SENSOR_ID"
          value = format("%s-%s-sensor", var.environment, var.location)
        }
      }
      service_account_name = google_service_account.sensor.email
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  metadata {
    labels = var.labels
  }

  depends_on = [
    google_service_account.sensor
  ]
}