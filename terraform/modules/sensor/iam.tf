resource "google_service_account" "sensor" {
  account_id   = format("sensor-%s-%s-sa", var.environment, var.location)
  display_name = format("sensor %s %s service account", var.environment, var.location)
}

resource "google_cloud_run_service_iam_binding" "binding" {
  location = google_cloud_run_service.default.location
  service  = google_cloud_run_service.default.name
  role     = "roles/run.invoker"
  members  = ["serviceAccount:${google_service_account.sensor.email}"]

  depends_on = [
    google_service_account.sensor,
    google_cloud_run_service.default
  ]
}

resource "google_pubsub_topic_iam_binding" "publish_permission" {
  topic   = var.topic
  role    = "roles/pubsub.publisher"
  members = ["serviceAccount:${google_service_account.sensor.email}"]

  depends_on = [
    google_service_account.sensor,
    data.terraform_remote_state.global
  ]
}