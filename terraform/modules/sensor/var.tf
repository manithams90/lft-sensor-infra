# variable "name" {
#   type        = string
#   description = "The user-provided description of the repository"
# }

variable "location" {
  type        = string
  description = "The location of the cloud run instance."
}

variable "project_id" {
  type        = string
  description = "ID of the Google Cloud project"
}

variable "labels" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}

variable "image" {
  description = " Docker image name"
  default     = "europe-docker.pkg.dev/lht-cloud-challenge-418023/sensor/sensor:latest"
  type        = string
}

variable "pubsub_id" {
  description = "The ID of the Pub/Sub topic"
  type        = string
}

variable "topic" {
  description = "The name of the Pub/Sub topic"
  type        = string
}

variable "environment" {
  description = "Infra Environment "
  type        = string
}