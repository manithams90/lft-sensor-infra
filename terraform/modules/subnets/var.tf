variable "project_id" {
  description = "ID of the Google Cloud project"
  default     = "lht-cloud-challenge-418023"
  type        = string
}

variable "resources_label" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}

variable "environment" {
  description = "Infra Environment "
  type        = string
}

variable "subnet_region" {
  type        = string
  description = "The region of the subnetwork."
}

variable "subnet_ip" {
  type        = string
  description = "The subnet to create"
}

variable "subnet_private_access" {
  type        = bool
  description = "Whether this subnet will have private Google access enabled"
}

variable "network_name" {
  type        = string
  description = "The name of the network where subnets will be created"
}