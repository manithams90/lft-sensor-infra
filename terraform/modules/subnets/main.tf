module "vpc_subnets" {
  source  = "terraform-google-modules/network/google//modules/subnets"
  version = "9.0.0"

  project_id   = var.project_id
  network_name = var.network_name

  subnets = [
    {
      subnet_name           = format("%s-%s", var.environment, var.subnet_region)
      subnet_ip             = var.subnet_ip
      subnet_region         = var.subnet_region
      subnet_private_access = var.subnet_private_access
    }
  ]
}