variable "location" {
  type        = string
  description = "The name of the location this repository is located in."
}

variable "repository_id" {
  type        = string
  description = "The last part of the repository name, for example: repo_1"
}

variable "description" {
  type        = string
  description = "The user-provided description of the repository"
}

variable "format" {
  type        = string
  description = "The format of packages that are stored in the repository."
}

variable "labels" {
  type        = map(string)
  description = "Labels with user-defined metadata."
}
