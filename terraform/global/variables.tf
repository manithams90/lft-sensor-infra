variable "project_id" {
  description = "ID of the Google Cloud project"
  default     = "lht-cloud-challenge-418023"
  type        = string
}

variable "region" {
  description = "Region of the resources"
  default     = "europe-west3"
  type        = string
}

variable "resources_label" {
  type        = map(string)
  default     = { "managed" = "terraform" }
  description = "label to identify service/purpose"
}