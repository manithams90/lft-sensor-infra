####################
## Enable Service ##
####################
module "project_services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 14.5"

  project_id                  = var.project_id
  disable_services_on_destroy = false

  activate_apis = [
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "artifactregistry.googleapis.com",
    "pubsub.googleapis.com",
    "compute.googleapis.com"
  ]
}

################################################
## Create Artifact Registry for docker images ##
################################################

module "artifact_registry_repository" {
  source        = "../modules/artifact-registry-repository"
  location      = "europe"
  repository_id = "sensor"
  description   = "Artifact Registry for sensor image"
  format        = "DOCKER"
  labels        = var.resources_label
}